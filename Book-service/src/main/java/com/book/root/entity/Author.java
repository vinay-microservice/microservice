package com.book.root.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name = "TB_EPAM_AUTHOR")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Author {
	
	@GeneratedValue(generator = "id_gen")
	@GenericGenerator(name = "id_gen", strategy = "com.book.root.utils.IdGenerator")
	@Column(name = "ID")
	@Id
	private String id;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "BOOK")
	@ManyToMany(mappedBy = "author")
	private List<Book> book;

	public Author(String name) {
		super();
		this.name = name;
	}
	
	
	

}
