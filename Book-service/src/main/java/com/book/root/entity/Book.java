package com.book.root.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.GenericGenerator;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "TB_EPAM_BOOK")
public class Book implements Serializable
	{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@GeneratedValue(generator = "id_gen")
	@GenericGenerator(name = "id_gen", strategy = "com.book.root.utils.IdGenerator")
	@Column(name = "ID")
	@Id
	private String id;
	
	@Column(name = "BOOK_NAME")
	private String name;
	
	@Column(name = "CATEGORY")
	private String category;
	
	@Column(name = "DISCRIPTION")
	private String discription;
	
	@Column(name="AUTHOR")
	@ManyToMany(cascade = {CascadeType.ALL})
	private List<Author> author;
	
	private String edition;

	public Book(String name, String category, String discription, List<Author> author, String edition) {
		super();
		this.name = name;
		this.category = category;
		this.discription = discription;
		this.author = author;
		this.edition = edition;
	}
	
	

}
