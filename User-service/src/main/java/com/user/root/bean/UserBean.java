package com.user.root.bean;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserBean {
	
	private String userName;
	
	private String email;
	

}
