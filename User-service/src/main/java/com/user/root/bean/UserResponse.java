package com.user.root.bean;

import java.util.List;

import lombok.Data;

@Data
public class UserResponse {
	
	private List<UserBean> userList;

}
