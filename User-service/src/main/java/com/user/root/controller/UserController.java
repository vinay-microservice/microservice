package com.user.root.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.user.root.bean.AddUserRequest;
import com.user.root.bean.UserBean;
import com.user.root.bean.UserResponse;
import com.user.root.service.UserService;

import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;

@RestController
@Api(value = "User Service")
@AllArgsConstructor
@RequestMapping(value = "/user")
public class UserController {

	private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

	private final UserService userService;

	@PostMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> addUser(@RequestBody AddUserRequest addUserRequest) {

		LOG.info("**** Entered UserController.addUser() ****");

		String res = userService.addUser(addUserRequest);

		return new ResponseEntity<>(res, HttpStatus.OK);
	}

	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserResponse> getAllUsers() {

		LOG.info("**** Entered UserController.getAllUsers() ****");
		UserResponse res = userService.getUserList();

		return new ResponseEntity<>(res, HttpStatus.OK);
	}

	@GetMapping(value = "/{userID}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserResponse> getByUserId(@PathVariable("userID") String userId) {

		LOG.info("**** Entered UserController.getByUserId() ****");
		UserBean userBean = userService.getByUserId(userId);
		UserResponse res = new UserResponse();
		List<UserBean> l = new ArrayList<>();
		if(userBean!=null) {
			l.add(userBean);
		}
		res.setUserList(l);

		return new ResponseEntity<>(res, HttpStatus.OK);
	}

	@DeleteMapping(value = "/{userID}")
	public ResponseEntity<String> deleteByUserId(@PathVariable("userID") String userId) {

		LOG.info("**** Entered UserController.deleteByUserId() ****");
		String res = userService.deleteByUserId(userId);
		return new ResponseEntity<>(res, HttpStatus.OK);
	}

	@PutMapping(value = "/{userID}")
	public ResponseEntity<String> updateByUserId(@PathVariable("userID") String userId, @RequestBody UserBean userBean) {

		LOG.info("**** Entered UserController.updateByUserId() ****");
		String res = userService.updateByUserId(userId,userBean);
		return new ResponseEntity<>(res, HttpStatus.OK);
	}

}
